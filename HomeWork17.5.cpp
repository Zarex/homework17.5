#include <iostream>

class MyClass
{

private:

    int a = 100500;
    
public:

    int GetA()
    {
        return a;
    }
    
};

class Vector
{
    
private:
    double x;
    double y;
    double z;

public:

    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << "Vector (" << x << ", " << y << ", " << z << ")" << "\n";
    }
    double Len()
    {
        double l = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << "Dlina vectora (" << x << ", " << y << ", " << z << ")" << " = " << l << std::endl;
        return 0;
    }


};


int main()
{
    
    MyClass test;
    std::cout << "a = " << test.GetA() << std::endl;

    Vector v(7,2,5);
    v.Show();
    v.Len();


}
